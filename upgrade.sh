#!/bin/bash

#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CHARTDIR=$DIR/src/
TMPDIR=/tmp/
VERSION=$(cat $CHARTDIR/Chart.yaml | grep ^version | awk '{print $2;}')
echo "Found chart version $VERSION" 
cd $CHARTDIR/
helm dependency update
mkdir -p $TMPDIR
cd $DIR/
helm package src
mv $DIR/ca.crypt-$VERSION.tgz $TMPDIR/ca.crypt-$VERSION.tgz
if [ -f "$DIR/values.yaml" ]; then
	echo "Found values.yaml"
else
	echo "values.yaml was not found - please copy the template and fill the required values"
	exit
fi

helm upgrade ca-crypt --namespace ca-crypt --values $DIR/values.yaml $TMPDIR/ca.crypt-$VERSION.tgz
