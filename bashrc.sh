klf() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  echo "kubectl logs -f --namespace=$NS $CONTAINER $2"
  kubectl logs -f --namespace=$NS $CONTAINER $2
}

kpa() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  kubectl exec --namespace=$NS -it $CONTAINER /bin/bash
}

kpas() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  kubectl exec --namespace=$NS -it $CONTAINER --container=$2 /bin/bash
}

kpassh() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  kubectl exec --namespace=$NS -it $CONTAINER --container=$2 /bin/sh
}


kd() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  kubectl describe pod --namespace=$NS $CONTAINER
}

kpd() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  kubectl delete pod --namespace=$NS $CONTAINER
}

kpash() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  kubectl exec --namespace=$NS -it $CONTAINER /bin/sh
}

kgcm() {
  kubectl get cm --all-namespaces
}
kdcm() {
  NS=$(kgcm | grep "$1" | head -n1 | awk -v N=1 '{print $N}')
  kubectl describe cm --namespace=$NS $1
}

alias k='kubectl get all --all-namespaces'
alias kp='kubectl get pod --all-namespaces'
alias kpe='kubectl get pod --all-namespaces | grep -v Running'
alias g='grep'

klfa() {
  CONTAINER=$(kubectl get pod --all-namespaces | grep " $1" | head -n1 | awk -v N=2 '{print $N}')
  NS=$(kubectl get pod --all-namespaces | grep "$CONTAINER" | head -n1 | awk -v N=1 '{print $N}')
  echo "kubectl logs -f --namespace=$NS $CONTAINER $2"
  kubectl logs -f --namespace=$NS $CONTAINER $2 app
}