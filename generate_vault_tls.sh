# SERVICE is the name of the Vault service in Kubernetes.
# It does not have to match the actual running service, though it may help for consistency.
SERVICE=ca-crypt-vault
# NAMESPACE where the Vault service is running.
NAMESPACE=ca-crypt
# SECRET_NAME to create in the Kubernetes secrets store.
SECRET_NAME=vault-server-tls
# TMPDIR is a temporary working directory.
TMPDIR=/tmp

openssl genrsa -out ${TMPDIR}/ca.key 2048
openssl req -x509 -new -nodes -key ${TMPDIR}/ca.key -subj "/CN=${MASTER_IP}" -days 10000 -out ${TMPDIR}/ca.crt
openssl genrsa -out ${TMPDIR}/vault.key 2048

cat <<EOF >${TMPDIR}/csr.conf
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
CN = ${SERVICE}.${NAMESPACE}.svc

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = ${SERVICE}
DNS.2 = ${SERVICE}.${NAMESPACE}
DNS.3 = ${SERVICE}.${NAMESPACE}.svc
DNS.4 = ${SERVICE}.${NAMESPACE}.svc.cluster.local
IP.1 = 127.0.0.1

[ v3_ext ]
authorityKeyIdentifier=keyid,issuer:always
basicConstraints=CA:FALSE
keyUsage=keyEncipherment,dataEncipherment
extendedKeyUsage=serverAuth,clientAuth
subjectAltName=@alt_names
EOF

openssl req -new -key ${TMPDIR}/vault.key -out ${TMPDIR}/vault.csr -config ${TMPDIR}/csr.conf

openssl x509 -req -in ${TMPDIR}/vault.csr -CA ${TMPDIR}/ca.crt -CAkey ${TMPDIR}/ca.key \
    -CAcreateserial -out ${TMPDIR}/vault.crt -days 10000 \
    -extensions v3_ext -extfile ${TMPDIR}/csr.conf -sha256

kubectl create secret generic ${SECRET_NAME} \
        --namespace ${NAMESPACE} \
        --from-file=vault.key=${TMPDIR}/vault.key \
        --from-file=vault.crt=${TMPDIR}/vault.crt \
        --from-file=vault.ca=${TMPDIR}/ca.crt

kubectl create configmap trusted-ca \
        --namespace ${NAMESPACE} \
        --from-file=vault.crt=${TMPDIR}/vault.crt \
        --from-file=vault.ca=${TMPDIR}/ca.crt